module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: "class",
  theme: {
    screens: {
      sm: "480px",
      md: "768px",
      lg: "976px",
      xl: "1440px",
    },
    colors: {
      transparent: "transparent",
      primary: "#151A4F",
      primaryDark: "#10154C",
      dark: "#090D37",
      secondary: "#ED1C24",
      error: "#EB5757",
      warning: "#E2B93B",
      success: "#27AE60",
      white: "#fff",
      back: {
        2: "#1D1D1D",
        3: "#282828",
      },
      gray: {
        1: "#333333",
        2: "#4F4F4F",
        3: "#828282",
        4: "#BDBDBD",
        5: "#E0E0E0",
        6: "#F7F7F7",
      },
    },
    fontSize: {
      H1: ["56px", "62px"],
      H2: ["48px", "53px"],
      H3: ["40px", "44px"],
      H4: ["32px", "36px"],
      H5: ["24px", "27px"],
      H6: ["20px", "22px"],
      small: ["14px", "20px"],
      regular: ["16.5px", "26px"],
      medium: ["19.5px", "26px"],
      large: ["21px", "26px"],
    },
    fontFamily: {
      sans: ["Outfit", "Graphik", "sans-serif"],
    },
    extend: {
      boxShadow: {
        main: "0 1px 0px rgba(237, 28, 36, 0.25)",
      },
      backgroundImage: (theme) => ({
        "banner-img": "url('/banner-image.png')",
      }),
    },
  },
  plugins: [
    require("@tailwindcss/typography"),
    // require("@tailwindcss/forms")
  ],
};
