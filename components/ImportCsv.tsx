import axios from "axios";
import React, { useState } from "react";
import { useRouter } from "next/router";
function App() {
  const router = useRouter();
  const [file, setFile] = useState();
  const [array, setArray] = useState([]);

  const fileReader = new FileReader();

  const handleOnChange = (e: any) => {
    setFile(e.target.files[0]);
    console.log(e.target.files[0]);
  };

  const csvFileToArray = (string: any) => {
    const csvHeader = string.slice(0, string.indexOf("\n")).split(",");
    const csvRows = string.slice(string.indexOf("\n") + 1).split("\n");

    const array = csvRows.map((i: any) => {
      const values = i.split(",");
      const obj = csvHeader.reduce(
        (
          object: { [x: string]: any },
          header: string | number,
          index: string | number
        ) => {
          object[header] = values[index];
          return object;
        },
        {}
      );
      return obj;
    });

    setArray(array);
  };

  const sendCsv = async (formData: any) => {
    return axios.post("/api", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  };

  const submitData = async (e: any) => {
    e.preventDefault();
    try {
      if (file) {
        let formData = new FormData();
        formData.append("theFiles", file);
        const data = await sendCsv(formData);
        console.log(data);
        router.push("/");
      }
    } catch (error) {
      console.log({ error });
    }
  };
  const handleOnSubmit = async (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.preventDefault();

    if (file) {
      fileReader.onload = function (event: any) {
        const text = event.target.result;
        csvFileToArray(text);
      };

      fileReader.readAsText(file);
    }
  };

  const headerKeys = Object.keys(Object.assign({}, ...array));

  return (
    <div style={{ textAlign: "center" }} className="mt-3">
      <form className="flex justify-center">
        <input
          type="file"
          id="csvFileInput"
          accept=".csv"
          onChange={handleOnChange}
        />

        <div className="flex space-x-2">
          <button
            className="bg-primary px-5 py-2 rounded-md text-white"
            onClick={(e) => {
              handleOnSubmit(e);
            }}
          >
            Preview csv
          </button>
          <button
            className="bg-primary px-5 py-2 rounded-md text-white"
            onClick={(e) => {
              submitData(e);
            }}
          >
            Upload csv
          </button>
        </div>
      </form>

      <br />

      <table>
        <thead>
          <tr key={"header"}>
            {headerKeys.map((key) => (
              <th>{key}</th>
            ))}
          </tr>
        </thead>

        <tbody>
          {array.map((item: any) => (
            <tr key={item.id}>
              {Object.values(item).map((val: any) => (
                <td>{val}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default App;
