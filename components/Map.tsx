import React from "react";
import { MapContainer, TileLayer, Marker, Popup, Tooltip } from "react-leaflet";
import "leaflet/dist/leaflet.css";
// import Loader from "./Loader";
import { Icon } from "leaflet";
const pointer = new Icon({
  iconUrl: "/map-pointer.svg",
  iconSize: [25, 25],
});

const Map = ({ map }: any) => {
  if (!map) {
    return (
      <div className="flex justify-center items-center">
        <div>Map is Loading......</div>
      </div>
    );
  }
  return (
    <React.Fragment>
      <MapContainer
        center={[9.082, 8.6753]}
        zoom={6}
        style={{ height: "80vh", width: "80vw" }}
        scrollWheelZoom={false}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {/* {!map && <div>Loading.......</div>} */}
        {map?.data?.map((val: any) => {
          return (
            <Marker
              // @ts-ignore: Unreachable code error
              position={val.location.coordinates}
              key={val.state}
              icon={pointer}
            >
              <Popup>
                <div className="flex flex-col">
                  <span>State: {val.state}</span>
                  <span>Precipitation: {val.precipitation}%</span>
                  <span>Temperature: {val.temperature} Celsius</span>
                </div>
              </Popup>
              <Tooltip>{val.state}</Tooltip>
            </Marker>
          );
        })}
      </MapContainer>
    </React.Fragment>
  );
};

export default Map;
