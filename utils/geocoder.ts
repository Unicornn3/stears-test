import NodeGeocoder from 'node-geocoder'

const options = {
  provider: process.env.GEOCODER_PROVIDER,
  httpAdapter: 'https',
  apiKey: process.env.GEOCODER_API_KEY,
  formatter: null
};

// @ts-ignore: Unreachable code error
const geocoder = NodeGeocoder(options);

module.exports = geocoder;
