import type { NextApiRequest, NextApiResponse } from 'next'
// import Weather from "../../models/Weather";
// import db from "../../utils/db";
import fs from 'fs';
import path from 'path'
import getConfig from 'next/config'

type Data = {
    name: string
}
const userData = {
    firstName: "Agbolade",
    lastName: "Adeniyi",
    gender: 'Male',
    Religion: 'Christianity'
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<any>
) {
    const { serverRuntimeConfig } = getConfig();

    const dirRelativeToPublicFolder = 'data/ng.csv'

    const dir = path.join(serverRuntimeConfig.PROJECT_ROOT, './', dirRelativeToPublicFolder);


    // fs.readFile('ng.csv', (err: any, data: any) => {
    //     if (err) {
    //         return console.error(err);
    //     }
    //     console.log("Asynchronous read: " + data.toString());
    // });
    console.log(process.cwd());



    res.status(200).json(userData)
}
