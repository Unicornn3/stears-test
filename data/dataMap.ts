import mapData from './nigeria.json'

const convertedData = () => {
    return {
        type: 'FeatureCollection',
        features: mapData.features.map((val) => {
            return {
                type: val.type,
                geometry: {
                    type: val.geometry.type,
                    coordinates: val.geometry.coordinates
                },
                properties: { ...val.properties }
            }
        })
    }
}

const stateObject = mapData.features.reduce((acc: any, cur) => {
    acc[cur.properties.NAME_1] = cur;
    return acc

}, {})

export const getWeatherData = (value: any): any => {
    return value.map(({ State, Temperature, Precipitation }: any) => {
        const obj: any = stateObject[State]
        if (obj !== undefined) {
            return {
                state: State,
                precipitation: Precipitation,
                temperature: Temperature,
                location: {
                    type: obj.geometry.type,
                    coordinates: obj.geometry.coordinates
                }

            }

        }
        return [];
    })
}
export const MAP_DATA = convertedData()
export const MAP_DATA_COUNT = convertedData().features.length



