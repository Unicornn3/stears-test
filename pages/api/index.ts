import type { NextApiRequest, NextApiResponse } from 'next'
// @ts-ignore: Unreachable code error
import Weather from "../../models/Weather";
import db from "../../utils/db";
import * as fs from 'fs';
const CSVToJSON = require('csvtojson');
import getConfig from 'next/config';
const { serverRuntimeConfig } = getConfig();
import { getWeatherData } from '../../data/dataMap'
import nc from 'next-connect'
import multer from 'multer';


const upload = multer({
    storage: multer.diskStorage({
        destination: '../../data',
        filename: (req, file, cb) => cb(null, 'ng.csv'),
    }),
});

const uploadMiddleware = upload.array('theFiles');

const handler = nc({

    onError: (err, req: NextApiRequest, res: NextApiResponse<any>) => {
        console.error(err.stack);
        res.status(500).end("Something broke!");
    },
    onNoMatch: (req, res) => {
        res.status(404).end("Page is not found");
    },
});

handler.use(uploadMiddleware)
    .get(async (req: NextApiRequest, res: NextApiResponse<any>) => {
        try {
            await db.connect()
            const rootPath = serverRuntimeConfig.PROJECT_ROOT
            const path = rootPath + '/data/ng.csv'
            const results = await CSVToJSON().fromFile(path);
            let data = getWeatherData(results);
            await Weather.deleteMany()
            await Weather.insertMany(data)
            data = await Weather.find({});
            await db.disconnect()
            return res.status(200).json({ count: data.length, data })
        } catch (error: any) {
            return res.send({ error: true, message: error.message })
        }
    });

handler.post(async (req: NextApiRequest, res: NextApiResponse<any>) => {
    try {
        return res.status(201).json({ message: "File uploade successfully" })
    } catch (error: any) {
        return res.send({ error: true, message: error })
    }
});


export default handler;

export const config = {
    api: {
        bodyParser: false, // Disallow body parsing, consume as stream
    },
};