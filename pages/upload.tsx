import dynamic from "next/dynamic";
import React from "react";
const ImportCsv = dynamic(() => import("../components/ImportCsv"), {
  ssr: false,
});
const upload = () => {
  return <ImportCsv />;
};

export default upload;
