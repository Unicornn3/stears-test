import mongoose from 'mongoose'
const geocoder = require('../utils/geocoder');

const weatherSchema = new mongoose.Schema({
  state: {
    type: String,
    required: [true, 'Please add a state']
  },
  precipitation: {
    type: Number,
    required: [true, 'Please add prepcipitation']
  },
  temperature: {
    type: Number,
    required: [true, 'Please add temperature']
  },
  location: {
    type: {
      type: String,
      enum: ['Polygon']
    },
    coordinates: {
      type: [Number],
      index: '2dsphere'
    },
    formattedAddress: String
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});
module.exports = mongoose.models.Weather || mongoose.model('Weather', weatherSchema);
